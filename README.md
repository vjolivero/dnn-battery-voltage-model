# DNN Battery voltage model

Project:  Intelligent system to estimate the behavior of the voltage at the terminals of a Lithium battery under different discharge profiles

notebooks: Baseline and validation model with data battery

graphics: Differente data visualization and results 

data: Dataset for discharge battery profile

Ing. Victor Jose Olivero Ortiz, MEng
DSI Student - Universidad del Norte 

Ing. Christian Giovanny Quintero Monroy, PhD 
Proffesor
Intelligent Design System - 
Universidad del Norte

